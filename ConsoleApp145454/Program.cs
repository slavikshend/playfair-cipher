﻿using ConsoleApp145454;
using System.Reflection;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.Write("Input keyword: ");
        string keyword = Console.ReadLine();
        if (keyword.Length > 25 || keyword.Distinct().Count() != keyword.Length)
        {
            Console.WriteLine("Error");
            Console.ReadKey();
            Environment.Exit(0);
        }
        Console.Write("Enter text: ");
        string text = Console.ReadLine();
        Playfair playfair = new Playfair();
        string cipher = playfair.Cipher(keyword, text);
        playfair.Decipher(cipher);
    }
}