﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp145454
{
    internal class Playfair
    {
        static string[,]? Matrix = new string[5,5];
        public static void CreateMatrix(string keyword)
        {
            string englishAlphabet = "ABCDEFGHIKLMNOPQRSTUVWXYZ";
            string changedAlphabet = keyword;
            foreach (char letter in englishAlphabet)
            {
                if (!changedAlphabet.Contains(letter))
                {
                    changedAlphabet += letter.ToString();
                }
            }
            string[,] matrix = new string[5,5];
            int alphabetIndex = 0;
            for (int row = 0; row < 5; row++)
            {
                for (int col = 0; col < 5; col++)
                {
                    if (alphabetIndex < englishAlphabet.Length)
                    {
                        matrix[row, col] = changedAlphabet[alphabetIndex].ToString();
                        alphabetIndex++;
                    }   
                }
            }
            Matrix = matrix;
        }
        public string Cipher(string keyword, string text)
        {
            string text_ = text.Replace(" ", "").ToUpper().Replace("J", "I");
            CreateMatrix(keyword);
            List<string> bigrams = new List<string>();
            for (int i = 0; i < text_.Length; i += 2)
            {
                if (i + 1 < text_.Length)
                {
                    string bigram = text_.Substring(i, 2);
                    if (bigram[0] == bigram[1])
                    {
                        bigram = bigram.Substring(0, 1) + "X";
                        i--; 
                    }
                    bigrams.Add(bigram);
                }
                else
                {
                    bigrams.Add(text_[i] + "X");
                }
            }

            foreach (string bigram in bigrams)
            {
                Console.WriteLine(bigram);
            }
            StringBuilder encryptedText = new StringBuilder();

            foreach (string bigram in bigrams)
            {
                string char1 = bigram[0].ToString();
                string char2 = bigram[1].ToString();
                int row1 = 0, col1 = 0, row2 = 0, col2 = 0;
                for (int row = 0; row < 5; row++)
                {
                    for (int col = 0; col < 5; col++)
                    {
                        if (Matrix[row, col] == char1)
                        {
                            row1 = row;
                            col1 = col;
                        }
                        if (Matrix[row, col] == char2)
                        {
                            row2 = row;
                            col2 = col;
                        }
                    }
                }
                string encryptedChar1, encryptedChar2;

                if (row1 == row2)
                {
                    encryptedChar1 = Matrix[row1, (col1 + 1) % 5];
                    encryptedChar2 = Matrix[row2, (col2 + 1) % 5];
                }
                else if (col1 == col2)
                {
                    encryptedChar1 = Matrix[(row1 + 1) % 5, col1];
                    encryptedChar2 = Matrix[(row2 + 1) % 5, col2];
                }
                else
                {
                    encryptedChar1 = Matrix[row1, col2];
                    encryptedChar2 = Matrix[row2, col1];
                }
                encryptedText.Append(encryptedChar1);
                encryptedText.Append(encryptedChar2);
            }
            for (int row = 0; row < 5; row++)
            {
                for (int col = 0; col < 5; col++)
                {
                    string element = Matrix[row, col];
                    Console.Write(element + " ");
                }
                Console.WriteLine(); 
            }
            Console.WriteLine(encryptedText.ToString());
            return encryptedText.ToString();
        }
        public string Decipher (string text)
        {
            StringBuilder decryptedText = new StringBuilder();
            List<string> bigrams = new List<string>();
            for (int i = 0; i < text.Length; i += 2)
            {
                string bigram = text.Substring(i, 2);
                bigrams.Add(bigram);
            }
            foreach (string bigram in bigrams)
            {
                string char1 = bigram[0].ToString();
                string char2 = bigram[1].ToString();

                int row1 = 0, col1 = 0, row2 = 0, col2 = 0;

                for (int row = 0; row < 5; row++)
                {
                    for (int col = 0; col < 5; col++)
                    {
                        if (Matrix[row, col] == char1)
                        {
                            row1 = row;
                            col1 = col;
                        }
                        if (Matrix[row, col] == char2)
                        {
                            row2 = row;
                            col2 = col;
                        }
                    }
                }

                string decryptedChar1, decryptedChar2;

                if (row1 == row2)
                {
                    decryptedChar1 = Matrix[row1, (col1 - 1 + 5) % 5];
                    decryptedChar2 = Matrix[row2, (col2 - 1 + 5) % 5];
                }
                else if (col1 == col2)
                {
                    decryptedChar1 = Matrix[(row1 - 1 + 5) % 5, col1];
                    decryptedChar2 = Matrix[(row2 - 1 + 5) % 5, col2];
                }
                else
                {
                    decryptedChar1 = Matrix[row1, col2];
                    decryptedChar2 = Matrix[row2, col1];
                }
                decryptedText.Append(decryptedChar1);
                decryptedText.Append(decryptedChar2);
            }
            Console.WriteLine(decryptedText.ToString());
            return decryptedText.ToString();
        }
    }
}
